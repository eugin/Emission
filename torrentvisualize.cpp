#include <QPixmap>
#include <QHeaderView>

#include "torrentvisualize.h"
#include "torrentinfo.h"
#include "titlecolumndelegate.h"

namespace titleColumns {
    static const int torrent = 2;
    static const int file = 0;
}


TorrentVisualizer::TorrentVisualizer(QTreeView * summaryView, QObject * parent)
    : QObject(parent)
    , summaryView(summaryView)
{
    summaryView->setItemDelegateForColumn(titleColumns::file, new TitleColumnDelegate(this));
    summaryView->setItemDelegateForColumn(titleColumns::torrent, new TitleColumnDelegate(this));

}

std::vector<QVariant> TorrentVisualizer::getHeader() const {
    //TODO: need translation
    return {TorrentInfo::id, TorrentInfo::status, TorrentInfo::name, TorrentInfo::rateDown, TorrentInfo::rateUp};
}

std::vector<QVariant> TorrentVisualizer::getTorrentsFields(const SummaryItem & torrentSummary) const {
    return {torrentSummary.id,
            statusImage(torrentSummary.status),
            titleCell(torrentSummary.name, torrentSummary.percentDone * torrentSummary.totalSize, torrentSummary.totalSize),
            humanSize(torrentSummary.rateDownload),
            humanSize(torrentSummary.rateUpload)};
}

std::vector<QVariant> TorrentVisualizer::getFileFields(const File & torrentFile) const {
    return {titleCell(torrentFile.name, torrentFile.bytesCompleted, torrentFile.length)};
}

QVariant TorrentVisualizer::statusImage(TorrentInfo::Status status) {
    switch (status) {
        case TorrentInfo::seed: return QPixmap(":/main/img/arrow-circle-top-4x.png");
        case TorrentInfo::download: return QPixmap(":/main/img/arrow-circle-bottom-4x.png");
        case TorrentInfo::check:
        case TorrentInfo::checkWait:
        case TorrentInfo::downloadWait:
        case TorrentInfo::seedWait:
        case TorrentInfo::stopped: break;
    }
    return QVariant();
}

QVariant TorrentVisualizer::titleCell(const QString & title, quint64 bytesCompleted, quint64 totalSize) const {
    QList<QVariant> content;
    content.append(title);
    content.append(humanSize(bytesCompleted));
    content.append(humanSize(totalSize));
    return content;
}

QString TorrentVisualizer::humanSize(quint64 bytes) const {
  QString text("bytes");
  if( !(bytes >> 10) ){
      return text.prepend(" ").prepend(QString::number(bytes));
  }
  if (! (bytes >> 20)) {
      double val = static_cast<double>(bytes)/1024;
      return text.prepend(" K").prepend(QString::number(val, 'f', 1));
  }
  if (! (bytes >> 30)) {
      double val = static_cast<double>(bytes)/1048576;
      return text.prepend(" M").prepend(QString::number(val, 'f', 1));
  }
  if (! (bytes >> 40)) {
      double val = static_cast<double>(bytes)/1073741824;
      return text.prepend(" G").prepend(QString::number(val, 'f', 1));
  }
  return text.prepend(" ").prepend(QString::number(bytes, 'f', 1));
}
