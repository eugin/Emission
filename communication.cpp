#include <QJsonDocument>

#include "communication.h"

namespace {
    const QJsonArray shownFields = {TorrentInfo::id, TorrentInfo::name, TorrentInfo::totalSize, TorrentInfo::percentDone, TorrentInfo::status, TorrentInfo::rateDown, TorrentInfo::rateUp, TorrentInfo::files};
}

namespace RequestMemebers {
    static const char * const arguments = "arguments";
    static const char * const method = "method";
    static const char * const tag = "tag";
}

namespace RequestArguments {
    static const char * const fields = "fields";
}

namespace RequestMethods {
    static const char * const get = "torrent-get";
}

namespace ParamFile {
    const static char * const name = "name";
    const static char * const bytesCompleted = "bytesCompleted";
    const static char * const length = "length";
}

namespace ReplyMemebers {
    static const char * const arguments = "arguments";
    static const char * const result = "result";
    static const char * const tag = "tag";
}

namespace ReplyResults {
    static const char * const success = "success";
}

namespace ReplyArguments {
    static const char * const torrents = "torrents";
}

Request::Request(int tag) : tag(tag) {
   QJsonObject arguments;
   arguments.insert(RequestArguments::fields, shownFields);
   QJsonObject top;
   top.insert(RequestMemebers::arguments, arguments);
   top.insert(RequestMemebers::method, RequestMethods::get);
   top.insert(RequestMemebers::tag, tag);
   QJsonDocument requestDocument(top);
   binaryData = requestDocument.toJson();
}

Response::Response(const QByteArray &binaryResponse) {
    QJsonDocument document = QJsonDocument::fromJson(binaryResponse);
    QJsonObject top = document.object();
    QJsonValue resultValue = top.value(ReplyMemebers::result);

    if (resultValue.toString() != ReplyResults::success) {
        TransmissionResponseError(resultValue.toString()).raise();
    }
    QJsonValue tag = top.value(ReplyMemebers::tag);
    this->tag = tag.toInt(-1);
    QJsonValue arguments = top.value(ReplyMemebers::arguments);
    QJsonObject argObject = arguments.toObject();
    QJsonValue torrentsValue = argObject.value(ReplyArguments::torrents);
    const QJsonArray & torrentsArray = torrentsValue.toArray();

    const int size = torrentsArray.size();
    items.resize(size);

    for (int i = 0; i < size; ++i) {
        const QJsonValue & item = torrentsArray[i];
        QJsonObject torrentObject = item.toObject();
        items[i].id = torrentObject.value(TorrentInfo::id).toInt();
        items[i].name = torrentObject.value(TorrentInfo::name).toString();
        items[i].rateDownload = torrentObject.value(TorrentInfo::rateDown).toInt();
        items[i].rateUpload = torrentObject.value(TorrentInfo::rateUp).toInt();
        items[i].status = static_cast<TorrentInfo::Status>(torrentObject.value(TorrentInfo::status).toInt());
        items[i].totalSize = torrentObject.value(TorrentInfo::totalSize).toVariant().toLongLong();
        items[i].percentDone = torrentObject.value(TorrentInfo::percentDone).toDouble();
        items[i].files = parseFiles(torrentObject);
    }
}

QVector<File> Response::parseFiles(const QJsonObject &torrentObject) {
    QJsonArray filesInfo = torrentObject.value(TorrentInfo::files).toArray();
    QVector<File> files;
    files.reserve(filesInfo.size());
    for (const QJsonValue & item : filesInfo) {
        QJsonObject fileInfo = item.toObject();
        File file;
        file.name = fileInfo.value(ParamFile::name).toString();
        file.bytesCompleted = fileInfo.value(ParamFile::bytesCompleted).toVariant().toLongLong();
        file.length = fileInfo.value(ParamFile::length).toVariant().toLongLong();
        files.append(qMove(file));
    }
    return files;
}


TransmissionResponseError::TransmissionResponseError(QString && errorString) : errorString(qMove(errorString)) {}

void TransmissionResponseError::raise() const {throw *this;}

TransmissionResponseError *TransmissionResponseError::clone() const {
    return new TransmissionResponseError(QString(errorString));
}
