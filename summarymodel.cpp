#include <QPixmap>
#include <iostream>

#include "summarymodel.h"
#include "util.h"

TreeItem::TreeItem(std::vector<QVariant> fields, int row, TreeItem * parent)
    : fields(std::move(fields))
    , parentItem(parent)
    , itemRow(row) {}

QVariant TreeItem::value(size_t column) const {
    return fields[column];
}

void TreeItem::addChild(TreeItem * child) {
    children.emplace_back(child);
}

TreeItem * TreeItem::parent() const {
    return parentItem;
}

TreeItem * TreeItem::child(size_t row) const {
    return children[row].get();
}

size_t TreeItem::childrenCount() const {
    return children.size();
}

bool TreeItem::hasChildren() const {
    return !children.empty();
}

size_t TreeItem::fieldsCount() const {
    return fields.size();
}

int TreeItem::row() const {
    return itemRow;
}

SummaryModel::SummaryModel(TransmissionTorrents & torrents, TorrentVisualizer & visualize, QObject* parent)
    : QAbstractItemModel(parent), torrents(torrents)
    , visualize(visualize)
    , rootItem(visualize.getHeader()) {

    connect(&torrents, SIGNAL(summaryUpdate(const Summary &)), SLOT(summaryUpdate(const Summary &)));
}

QModelIndex SummaryModel::index(int row, int column, const QModelIndex & parent) const {
    if (!hasIndex(row, column, parent))
          return QModelIndex();

    if (!parent.isValid()) {
        return createIndex(row, column, rootItem.child(row));
    }

    TreeItem * parentItem = static_cast<TreeItem *>(parent.internalPointer());
    if (!parentItem->hasChildren()) {
        return QModelIndex();
    }

    Q_ASSERT(parentItem->child(row) != nullptr);
    return createIndex(row, column, parentItem->child(row));
}

QModelIndex SummaryModel::parent(const QModelIndex & index) const {
    if (!index.isValid()) {
        return QModelIndex();
    }

    TreeItem * currentItem = static_cast<TreeItem *>(index.internalPointer());
    TreeItem * parent = currentItem->parent();
    if (parent == &rootItem) {
        return QModelIndex();
    }

    return createIndex(parent->row(), 0, parent);
}

int SummaryModel::rowCount(const QModelIndex &parent) const {
    if (!rootItem.hasChildren() || parent.column() > 0) {
        return 0;
    }

    if (!parent.isValid()) {
       return int(rootItem.childrenCount());
    }

    TreeItem * parentDetails = static_cast<TreeItem *>(parent.internalPointer());
    return int(parentDetails->childrenCount());
}

int SummaryModel::columnCount(const QModelIndex &parent) const {
    if (!parent.isValid()) {
        return int(rootItem.fieldsCount());
    }

    TreeItem * parentDetails = static_cast<TreeItem *>(parent.internalPointer());
    return int(parentDetails->child(0)->fieldsCount());
}

QVariant SummaryModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
            return rootItem.value(section);

        return QVariant();
}

QVariant SummaryModel::data(const QModelIndex &index, int role) const {
    switch(role) {
        case Qt::DisplayRole:return displayText(index);
        case Qt::DecorationRole: return displayImage(index);
//        case Qt::UserRole: return ids.at(index.row());
    }

    return QVariant();
}

QVariant SummaryModel::displayText(const QModelIndex &index) const {
    if (!index.isValid()) {
        return QVariant();
    }
    TreeItem * item = static_cast<TreeItem*>(index.internalPointer());
    return item->value(index.column());
}

QVariant SummaryModel::displayImage(const QModelIndex &index) const {
    if (!index.isValid()) {
        return QVariant();
    }

    if(!index.parent().isValid() && index.column() == 1) { //TODO: use constants
        return rootItem.child(index.row())->value(index.column());
    }
    return QVariant();
}

void SummaryModel::updateModel() {
    torrents.getSummary();
}

void SummaryModel::summaryUpdate(const Summary & summary) {
    beginResetModel();

    int row = 0;
    for (const SummaryItem & item : summary.items) {
        TreeItem * torrentItem = new TreeItem(visualize.getTorrentsFields(item), row++, &rootItem);
        for (int fileRow = 0; fileRow < item.files.size(); fileRow++) {
            torrentItem->addChild(new TreeItem(visualize.getFileFields(item.files[fileRow]), fileRow, torrentItem));
        }
        rootItem.addChild(torrentItem);
    }
    endResetModel();
}
