#ifndef MAINWINDOWMODEL_H
#define MAINWINDOWMODEL_H

#include <QObject>
#include <QAbstractItemModel>

#include "transmissiontorrents.h"
#include "torrentvisualize.h"
#include "summarymodel.h"

class MainWindowModel : public QObject
{
    TransmissionTorrents torrents;
    SummaryModel mainModel;

    Q_OBJECT

public:
    explicit MainWindowModel(TorrentVisualizer & visualize, QObject *parent = 0);

    void hostChanged(const QString& host);
    void updateDetails(const QModelIndex &index);
    SummaryModel *getMainTableModel();

signals:

public slots:
};

#endif // MAINWINDOWMODEL_H
