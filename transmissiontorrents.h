#ifndef TRANSMISSIONTORRENTS_H
#define TRANSMISSIONTORRENTS_H

#include <QObject>
#include <QVector>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "communication.h"

class TransmissionTorrents : public QObject // TODO: request only
{
    Q_OBJECT
    QString host;
    QString sessionId;
    QNetworkAccessManager networkAccess;
    QVector<int> ids;
    QMap<int, Request> requests;
    int lastTag;

public:
    const static int fields;

    explicit TransmissionTorrents(QObject *parent = 0);
    void setHost (const QString& host);
    void getSummary();

signals:
    void summaryUpdate(const Summary & torrentsInfo);

private slots:
    void replyReceived(QNetworkReply *reply);

private:
    bool checkResponseCode(QNetworkReply *reply);
    void sendRequest(Request &request);
    void processResponse(const Response & response);
};

#endif // TRANSMISSIONTORRENTS_H
