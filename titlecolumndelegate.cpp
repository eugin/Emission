#include "titlecolumndelegate.h"

#include <QStyleOptionProgressBar>
#include <QRect>
#include <QApplication>

#include "summarymodel.h"

TitleColumnDelegate::TitleColumnDelegate(QObject* parent)
    : QAbstractItemDelegate(parent) {
}

void TitleColumnDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    if (!index.parent().isValid() && index.column() != 2) {
        return;
    }

    QList<QVariant> data = index.data().toList();
    auto it = data.begin();
    QString name = it->toString(); ++it;
    QString completed = it->toString(); ++it;
    QString totalSize = it->toString(); ++it;

    QStyleOptionProgressBar progressBar;
    QRect rect = option.rect;
    int x, y, w, h;
    x = rect.x();
    w = rect.width();
    y = rect.y() + rect.height()/2;
    h = rect.height()/2;
    progressBar.rect = QRect(x, y, w, h);
    progressBar.maximum = 100;
    progressBar.minimum = 0;
    progressBar.progress = 100;//int((bytesCompleted * 100ULL) / totalSize); TODO: need actual data

    QString progressText(QString::number(progressBar.progress) + "%");
    progressText.append(" (").append(completed).append("/").append(totalSize).append(")");
    progressBar.text = progressText;
    progressBar.textVisible = true;
    QRect textRect(rect.x(),rect.y(), rect.width(), rect.height() / 2);

    QApplication::style()->drawItemText(painter, textRect, Qt::AlignCenter, QPalette(), true, name);
    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBar, painter);
}

QSize TitleColumnDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const {
    return QSize();
}

