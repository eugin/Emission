#include "mainwindow.h"
#include "titlecolumndelegate.h"

#include <QSortFilterProxyModel>
#include <QAbstractItemModel>
#include <QHeaderView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
    setupMainWidgets();
}

void MainWindow::hostChanged() {
    model->hostChanged(combo->currentText());
}

void MainWindow::torrentExpanded(const QModelIndex & index) {
    int rows = model->getMainTableModel()->rowCount(index);
    for (int i = 0; i < rows; ++i) {
        summaryView->setFirstColumnSpanned(i, index, true);
    }
}

void MainWindow::setupMainWidgets() {
    setupSummaryView();
    setupToolbar();
    setupCombo();
}

void MainWindow::setupSummaryView() {
    summaryView = findChild<QTreeView*>("treeView");
    summaryView->setSelectionBehavior(QAbstractItemView::SelectRows);
    visualizer = new TorrentVisualizer(summaryView, this);
    model = new MainWindowModel(*visualizer, this);
    SummaryModel * summaryModel = model->getMainTableModel();
    summaryView->setModel(summaryModel);
    summaryView->header()->setSectionResizeMode(2, QHeaderView::Stretch);
    summaryView->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
}

void MainWindow::setupToolbar() {
    toolBar = findChild<QToolBar*>("toolBar");
    toolBar->addAction("Add");
    toolBar->addAction("Remove");
}

void MainWindow::setupCombo() {
    combo = findChild<QComboBox*>("comboBox");
    connect(combo, SIGNAL(currentIndexChanged(int)), SLOT(hostChanged()));
    combo->insertItem(0, "192.168.0.104"); //TODO: file nedded
}

