#ifndef TITLECOLUMNDELEGATE_H
#define TITLECOLUMNDELEGATE_H

#include <QAbstractItemDelegate>

#include "summarymodel.h"

class TitleColumnDelegate : public QAbstractItemDelegate
{
public:
  TitleColumnDelegate(QObject *parent = 0);

  virtual void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const ;
  virtual QSize sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const;
};

#endif // TITLECOLUMNDELEGATE_H
