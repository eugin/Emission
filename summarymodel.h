#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include <memory>

#include "transmissiontorrents.h"
#include "torrentvisualize.h"

class TreeItem {
public:
    TreeItem(std::vector<QVariant> fields, int row = 0, TreeItem * parent = nullptr);
    TreeItem(TreeItem &&) = default;

    QVariant value(size_t column) const;
    void addChild(TreeItem * child);
    TreeItem * parent() const;
    TreeItem * child(size_t row) const;
    size_t childrenCount() const;
    bool hasChildren() const;
    size_t fieldsCount() const;
    int row() const;

protected:
    std::vector<QVariant> fields;
    std::vector<std::unique_ptr<TreeItem>> children;
    TreeItem * parentItem = nullptr;
    int itemRow = 0;
};

class SummaryModel : public QAbstractItemModel {
    Q_OBJECT

    static const int columns = 3;

    TransmissionTorrents & torrents;
    TorrentVisualizer & visualize;
    TreeItem rootItem;

public:
    SummaryModel(TransmissionTorrents & torrents, TorrentVisualizer & visualize, QObject *parent = 0);

    int rowCount(const QModelIndex &) const override;
    int columnCount(const QModelIndex &) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;
    void updateModel();

public slots:
    void summaryUpdate(const Summary & summary);

private:
    QVariant displayText(const QModelIndex& index) const;
    QVariant displayImage(const QModelIndex& index) const;
};

#endif // TABLEMODEL_H
