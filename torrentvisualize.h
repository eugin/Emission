#ifndef TORRENTVISUALIZE_H
#define TORRENTVISUALIZE_H

#include <QString>
#include <QVariant>
#include <QTreeView>

#include <vector>

#include "torrentinfo.h"

class TorrentVisualizer : public QObject {
public:
    TorrentVisualizer(QTreeView * summaryView, QObject * parent);
    std::vector<QVariant> getHeader() const;
    std::vector<QVariant> getTorrentsFields(const SummaryItem & torrentSummary) const;
    std::vector<QVariant> getFileFields(const File & torrentFile) const;
    void prepareSummaryView(QTreeView * torrentsView);

private:
    static QVariant statusImage(TorrentInfo::Status status);
    QVariant titleCell(const QString& title, quint64 bytesCompleted, quint64 totalSize) const;
    QString humanSize(quint64 bytes) const;

private:
    QTreeView * summaryView;
};

#endif // TORRENTVISUALIZE_H
