#include "transmissiontorrents.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QByteArray>

TransmissionTorrents::TransmissionTorrents(QObject *parent) : QObject(parent),lastTag(0) {
    connect(&networkAccess, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyReceived(QNetworkReply*)));
}

void TransmissionTorrents::setHost(const QString &host) {
    this->host.append("http://");
    this->host.append(host);
    this->host.append(":9091/transmission/rpc");
}

void TransmissionTorrents::replyReceived(QNetworkReply* reply) {
    if (!checkResponseCode(reply)) {
        sendRequest(requests.first());
        return;
    }

    QByteArray replyData = reply->readAll();
    try {
        Response response(replyData);
        processResponse(response);
    } catch(TransmissionResponseError & ex) {
        //handle error
    }
    reply->deleteLater();
}

void TransmissionTorrents::processResponse(const Response &response) {
    if (requests.find(response.requestTag()) == requests.end()) {
        return;
    }

    requests.remove(response.requestTag());
    emit summaryUpdate(response);
}

bool TransmissionTorrents::checkResponseCode(QNetworkReply* reply) {
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    bool ok;
    int code = statusCode.toInt(&ok);
    if ( !ok ) {
        return false;
    }

    switch (code) {
        case 200: return true;
        case 409: sessionId.append(reply->rawHeader("X-Transmission-Session-Id"));
        default: return false;
    }
}

void TransmissionTorrents::sendRequest(Request & request) {
    QNetworkRequest networkRequest(host);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain");
    networkRequest.setRawHeader(QString("X-Transmission-Session-Id").toLocal8Bit(), sessionId.toLocal8Bit());
    networkAccess.post(networkRequest, request.requestData());
    requests.insert(request.requestTag(), std::move(request));
}

void TransmissionTorrents::getSummary() {
    Request request(++lastTag);
    sendRequest(request);
}
