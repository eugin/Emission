#include "mainwindowmodel.h"

MainWindowModel::MainWindowModel(TorrentVisualizer & visualize, QObject *parent)
    : QObject(parent)
    , torrents()
    , mainModel(torrents, visualize) {}

void MainWindowModel::hostChanged(const QString &host) {
    torrents.setHost(host);
    mainModel.updateModel();
}

SummaryModel *MainWindowModel::getMainTableModel() {
    return &mainModel;
}
