#pragma once

#include <QMap>
#include <QVector>
#include <QVariant>
#include <QJsonArray>
#include <QJsonObject>
#include <QException>

#include "torrentinfo.h"

class Request {
public:
    DefaultCopyMove(Request);
    Request(int tag);
    QByteArray requestData() const {return binaryData;}
    int requestTag() const {return tag;}

private:

    QByteArray binaryData;
    int tag;
};

class Response : public Summary {
public:
    DefaultCopyMove(Response);
    Response(const QByteArray & binaryResponse);
    int requestTag() const {return tag;}

private:
    QVector<File> parseFiles(const QJsonObject & torrentObject);

private:
    int tag;
};


class TransmissionResponseError : public QException {
public:
    TransmissionResponseError(QString && errorString);
    void raise() const override;
    TransmissionResponseError * clone() const override;

public:
    const QString errorString;
};
