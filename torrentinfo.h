#pragma once

#include <QString>
#include <QVector>

#define DefaultCopyMove(className) \
    className (const className &) = default; \
    className (className &&) = default; \
    className & operator = (const className &) = default; \
    className & operator = (className &&) = default


namespace TorrentInfo {
    constexpr static const char * const id = "id";
    constexpr static const char * const name = "name";
    constexpr static const char * const totalSize = "totalSize";
    constexpr static const char * const percentDone = "percentDone";
    constexpr static const char * const status = "status";
    constexpr static const char * const rateDown = "rateDownload";
    constexpr static const char * const rateUp = "rateUpload";
    constexpr static const char * const files = "files";

    enum Status {
        stopped        = 0, /* Torrent is stopped */
        checkWait     = 1, /* Queued to check files */
        check          = 2, /* Checking files */
        downloadWait  = 3, /* Queued to download */
        download       = 4, /* Downloading */
        seedWait      = 5, /* Queued to seed */
        seed           = 6  /* Seeding */
    };
}

struct File {
    QString name;
    quint64 bytesCompleted = 0;
    quint64 length = 0;

    File() {}
    DefaultCopyMove(File);
};

struct SummaryItem {
    int id = 0;
    QString name;
    quint64 totalSize = 0;
    double percentDone = 0;
    TorrentInfo::Status status;
    quint32 rateDownload = 0;
    quint32 rateUpload = 0;
    QVector<File> files;

    SummaryItem() {}
    DefaultCopyMove(SummaryItem);
};


struct Summary {
    QVector<SummaryItem> items;

    Summary() {}
    DefaultCopyMove(Summary);
};
