#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableView>
#include <QComboBox>
#include <QThread>
#include <QToolBar>
#include <QTreeView>
#include <QScopedPointer>

#include "mainwindowmodel.h"
#include "ui_mainwindow.h"
#include "summarymodel.h"
#include "torrentvisualize.h"

class MainWindow : public QMainWindow {
  Q_OBJECT

  QScopedPointer<Ui::MainWindow> ui;
  QTreeView *summaryView;
  QComboBox *combo;
  QToolBar* toolBar;
  TorrentVisualizer * visualizer;
  MainWindowModel * model;

public:
  explicit MainWindow(QWidget *parent = 0);

private slots:
  void hostChanged();
  void torrentExpanded(const QModelIndex & index);
  void setupMainWidgets();

signals:
  void connectHost(const QString& host);

private:
  void setupSummaryView();
  void setupToolbar();
  void setupCombo();

};

#endif // MAINWINDOW_H
