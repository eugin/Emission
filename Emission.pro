#-------------------------------------------------
#
# Project created by QtCreator 2015-11-14T18:26:27
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Emission
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    transmissiontorrents.cpp \
    communication.cpp \
    titlecolumndelegate.cpp \
    mainwindowmodel.cpp \
    summarymodel.cpp \
    torrentvisualize.cpp

HEADERS  += mainwindow.h \
    transmissiontorrents.h \
    communication.h \
    torrentinfo.h \
    titlecolumndelegate.h \
    mainwindowmodel.h \
    summarymodel.h \
    torrentvisualize.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY =

RESOURCES += \
    resources.qrc

QMAKE_CXXFLAGS += -std=c++11
